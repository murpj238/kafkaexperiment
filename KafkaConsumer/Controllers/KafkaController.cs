﻿using System.Collections.Generic;
using System.Threading;
using Confluent.Kafka;
using KafkaConsumer.Models;
using Microsoft.AspNetCore.Mvc;

namespace KafkaConsumer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class KafkaController : ControllerBase
    {
        

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<KafkaMessage>> Get()
        {
            var result = new List<KafkaMessage>();
            var conf = new ConsumerConfig
            {
                BootstrapServers = "10.139.80.97:9092",
                GroupId = "csharp-consumer",
                EnableAutoCommit = false,
                StatisticsIntervalMs = 5000,
                SessionTimeoutMs = 6000,
                AutoOffsetReset = AutoOffsetReset.Earliest,
                EnablePartitionEof = true

            };
            using (var consumer = new ConsumerBuilder<Ignore, string>(conf)
                // Note: All handlers are called on the main .Consume thread.
                .SetErrorHandler((_, e) => result.Add(new KafkaMessage { Message = $"Error: {e.Reason}"}))
                .SetStatisticsHandler((_, json) => result.Add(new KafkaMessage { Message = $"Statistics: {json}"}))
                .Build())
            {
                consumer.Subscribe("test");
                try
                {

                    var cr = consumer.Consume(CancellationToken.None);
                    while (!cr.IsPartitionEOF)
                    {
                        result.Add(new KafkaMessage { Message = cr.Value});
                        cr = consumer.Consume(CancellationToken.None);
                    }
                }
                catch (ConsumeException e)
                {
                    result.Add(new KafkaMessage { Message = $"Error occured: {e.Error.Reason}"});
                }
                finally
                {
                    consumer.Close();
                }
            }
            return new ActionResult<IEnumerable<KafkaMessage>>(result);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
